% Copyright © 2021-2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Jan Adelmann, Roman Vetter & Dagmar Iber

function fig4b

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
nruns = 1000; % number of independent simulation runs
nboot = 1e4; % number of bootstrap samples for error estimation
diameter = 5; % cell diameter [µm]
mu_D = 0.033; % mean morphogen diffusion coefficient [µm^2/s]
mu_lambda = 20; % mean gradient length [µm]
ncS = 5; % number of cells in the source domain
ncP = 50; % number of cells in the patterning domain
CV = 0.3; % coefficient of variation for the kinetic parameters
CV_A = 0.5; % coefficient of variation for the cell area

LS = ncS * diameter; % source length
LP = ncP * diameter; % pattern length

CVfun = @(x) nanstd(x) ./ nanmean(x);
SEfun = @(x) nanstd(x) ./ sqrt(sum(~isnan(x)));

% Variation in kinetic parameters lets the mean gradient length drift (Vetter & Iber, 2021).
% To control the ratio mu_d/mu_lambda, mu_lambda needs to be adjusted, such that
% the effective fitted mu_lambda is constant.
mu_lambda_adjusted = mu_lambda / (1 - 0.011 * CV + 1.355 * CV^2 - 0.179 * CV^3 + 0.0077 * CV^4)^-0.357;

% define degradation and production rate
mu_d = mu_D/mu_lambda_adjusted^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]

diameters = [mu_lambda/40, mu_lambda/4, mu_lambda/2]; % change the average diameter of the cell

readout_pos = linspace(0,LP,101);

% create folder to save files in if it does not already exist
dir = 'fig4b';
if not(isfolder(dir))
    mkdir(dir)
end

%% effect of noise and cell size on concentration readout at predfined readout positions with varying mean diameters

% loop over the different average cell sizes
for diam = diameters

    % define output file names
    filename_centroid = [dir '/read_out_precision_centroid_conc_' num2str(diam) '.csv'];
    filename_random = [dir '/read_out_precision_random_conc_' num2str(diam) '.csv'];
    filename_average = [dir '/read_out_precision_average_conc_' num2str(diam) '.csv'];

    % allocate memory
    conc_average = NaN(length(CV), length(readout_pos));
    conc_SE_average = NaN(length(CV), length(readout_pos));
    conc_CV_average = NaN(length(CV), length(readout_pos));
    conc_CV_SE_average = NaN(length(CV), length(readout_pos));
    conc_random = NaN(length(CV), length(readout_pos));
    conc_SE_random = NaN(length(CV), length(readout_pos));
    conc_CV_SE_random = NaN(length(CV), length(readout_pos));
    conc_CV_random = NaN(length(CV), length(readout_pos));
    conc_centroid = NaN(length(CV), length(readout_pos));
    conc_SE_centroid = NaN(length(CV), length(readout_pos));
    conc_CV_centroid = NaN(length(CV), length(readout_pos));
    conc_CV_SE_centroid = NaN(length(CV), length(readout_pos));

    conc_per_iteration_average = NaN(nruns, length(readout_pos));
    conc_per_iteration_random = NaN(nruns, length(readout_pos));
    conc_per_iteration_centroid = NaN(nruns, length(readout_pos));

    % loop over several independent runs
    for j = 1:nruns

        % build the domain with variable cell sizes
        [l_s, l_p] = helper_functions.build_domain(LS, LP, diam, CV_A);

        % create grid for the solver
        x0 = [-l_s, 0, l_p];
        x0 = sort([x0 x0(2:end-1)]); % duplicate cell boundary nodes

        ncS = length(l_s);
        ncP = length(l_p);
        nc = ncS + ncP;

        options = bvpset('Vectorized', 'on', 'NMax', 100*nc, 'RelTol', tol, 'AbsTol', tol);

        % draw random kinetic parameters for each cell
        p = random(helper_functions.logndist(mu_p, CV), nc, 1);
        d = random(helper_functions.logndist(mu_d, CV), nc, 1);
        D = random(helper_functions.logndist(mu_D, CV), nc, 1);

        % get initial solution
        sol0 = bvpinit(x0, @helper_functions.y0);

        % define new function handles for boundary condition
        % function and initial condition function
        bcfun_init = @(ya, yb) helper_functions.bcfun(ya, yb, nc);

        % define function handle for the diffusion equation
        odefun_init = @(x,y,c) helper_functions.odefun(x, y, c, D, p, d, ncS);

        % solve the equation
        sol = bvp4c(odefun_init, bcfun_init, sol0, options);

        % initialise the start location at the beginning of the
        % patterning domain
        cell_beginning = 0;

        % arrays to store readout concentrations for each cell
        C_average = NaN(1,ncP);
        C_random = NaN(1,ncP);
        C_centroid = NaN(1,ncP);

        % use the beginning of each cell as the x-coordinate
        cell_beginnings = [0, l_p(1:end-1)];

        % calculate the midpoint of each cell
        midpoint = (cell_beginnings + l_p) / 2;

        % loop through the cells in the patterning domain
        for c = 1:ncP

            % set the upper interval as the end of a cell
            cell_end = l_p(c);

            % define interval where to extract solutions
            logical_indexes = (sol.x <= cell_end) & (sol.x >= cell_beginning);

            % get lenght of the cell for normalisation
            cell_length = cell_end - cell_beginning;

            % get the x and y solution
            X = sol.x(logical_indexes);
            Y = sol.y(1, logical_indexes);

            % get unique x, y values for interpolation
            x_unique = unique(X, 'stable');
            y_unique = unique(Y, 'stable');

            % increase the resoultion of points in each cell
            x_high_res = linspace(cell_beginning, cell_end, 100);

            % get interpolated solutions for better resolution
            y_high_res = pchip(x_unique, y_unique, x_high_res);

            % interpolation um Mittelwert zu bestimmen
            C_centroid(c) = pchip(x_high_res, y_high_res, midpoint(c));

            % get a random point in the cell
            rand_x = (cell_end-cell_beginning) * rand + cell_beginning;

            % get solution at that point
            C_random(c) = pchip(x_high_res, y_high_res, rand_x);

            % get the average concentration per cell
            C_average(c) = trapz(x_high_res, y_high_res) / cell_length;

            % set the lower interval for the next iteration as the
            % current end of the cell
            cell_beginning = cell_end;

        end

        % array to store the concenctration at each readout_pos x
        piecewise_const_average = NaN(1, length(readout_pos));
        piecewise_const_random = NaN(1, length(readout_pos));
        piecewise_const_centroid = NaN(1, length(readout_pos));

        % loop through readout positions to get the concentration c(x) at each x value
        for idx = 1:length(readout_pos)

            % if at the end, set the index to the position of the last cell
            if readout_pos(idx) >= cell_beginnings(end)

                B = length(cell_beginnings);

                piecewise_const_average(1, idx) = C_average(B);
                piecewise_const_random(1, idx) = C_random(B);
                piecewise_const_centroid(1, idx) = C_centroid(B);

            else

                % Find indices where the readout position is smaller than the starting point of a cell.
                % The index on the left defines the value of of the concentration c(x).

                B = find(readout_pos(idx) < cell_beginnings, 1) - 1;

                piecewise_const_average(1, idx) = C_average(B);
                piecewise_const_random(1, idx) = C_random(B);
                piecewise_const_centroid(1, idx) = C_centroid(B);

            end

        end

        % get all concentrations for one iteration at each location x
        conc_per_iteration_average(j, :) = piecewise_const_average;
        conc_per_iteration_random(j, :) = piecewise_const_random;
        conc_per_iteration_centroid(j, :) = piecewise_const_centroid;

    end

    % Caculate summary statistics for the nruns
    % calculate the mean concentration at each position
    conc_average(1, :) = mean(conc_per_iteration_average);
    conc_random(1, :) = mean(conc_per_iteration_random);
    conc_centroid(1, :) = mean(conc_per_iteration_centroid);

    % calculate the standard error at each position
    conc_SE_average(1, :) = SEfun(conc_per_iteration_average);
    conc_SE_random(1, :) = SEfun(conc_per_iteration_random);
    conc_SE_centroid(1, :) = SEfun(conc_per_iteration_centroid);

    % calculate the coefficient of variation at each position
    conc_CV_average(1, :) = CVfun(conc_per_iteration_average);
    conc_CV_random(1, :) = CVfun(conc_per_iteration_random);
    conc_CV_centroid(1, :) = CVfun(conc_per_iteration_centroid);

    % calculate the SE for the coefficient of variation
    conc_CV_SE_average(1, :) = std(bootstrp(nboot, CVfun, conc_per_iteration_average));
    conc_CV_SE_random(1, :) = std(bootstrp(nboot, CVfun, conc_per_iteration_random));
    conc_CV_SE_centroid(1, :) = std(bootstrp(nboot, CVfun, conc_per_iteration_centroid));

    readout_pos_scaled = readout_pos / mu_lambda;

    writetable(table(readout_pos_scaled', conc_average', conc_SE_average', conc_CV_average', conc_CV_SE_average', 'VariableNames', {'pos', 'conc_average','conc_SE_average', 'conc_CV_average', 'conc_CV_SE_average'}), filename_average);
    writetable(table(readout_pos_scaled', conc_centroid', conc_SE_centroid', conc_CV_centroid', conc_CV_SE_centroid', 'VariableNames', {'pos', 'conc_centroid','conc_SE_centroid', 'conc_CV_centroid', 'conc_CV_SE_centroid'}), filename_centroid);
    writetable(table(readout_pos_scaled', conc_random', conc_SE_random', conc_CV_random', conc_CV_SE_random', 'VariableNames', {'pos', 'conc_random','conc_SE_random', 'conc_CV_random', 'conc_CV_SE_random'}), filename_random);

end

end
