% Copyright © 2021-2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Jan Adelmann, Roman Vetter & Dagmar Iber

function fig2gh

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
nruns = 1000; % number of independent simulation runs
nboot = 1e4; % number of bootstrap samples for error estimation
diameter = 5; % cell diameter [µm]
mu_D = 0.033; % mean morphogen diffusion coefficient [µm^2/s]
ncS = 5; % number of cells in the source domain
ncP = 50; % number of cells in the patterning domain
CV = 0.3; % coefficient of variation for the kinetic parameters
CV_A = 0.5; % coefficient of variation for the cell area
mu_lambdas = linspace(5,50,19); % gradient decay lengths [µm]

LS = ncS * diameter; % source length
LP = ncP * diameter; % pattern length

CVfun = @(x) nanstd(x) / nanmean(x);
SEfun = @(x) nanstd(x) / sqrt(sum(~isnan(x)));

fitopt = statset('TolFun', tol, 'TolX', tol);

% k = p, d, D, and all three together
names = {'p', 'd', 'D', 'all'};

dir = 'fig2gh';
if not(isfolder(dir))
    mkdir(dir)
end

for k = 1:numel(names)

    filename = [dir '/Mean_lambda_change_vs_CV_' names{k} '.csv'];
    if names{k} == 'D'
        filename = [dir '/Mean_lambda_change_vs_CV_Diff.csv'];
    end

    lambda = NaN(length(mu_lambdas), 1);
    lambda_SE = NaN(length(mu_lambdas), 1);
    C0 = NaN(length(mu_lambdas), 1);
    C0_SE = NaN(length(mu_lambdas), 1);
    CV_lambda = NaN(length(mu_lambdas), 1);
    CV_lambda_SE = NaN(length(mu_lambdas), 1);
    CV_0 = NaN(length(mu_lambdas), 1);
    CV_0_SE = NaN(length(mu_lambdas), 1);

    % loop over cell diameters
    for i = 1:length(mu_lambdas)

        % Variation in kinetic parameters lets the mean gradient length drift (Vetter & Iber, 2021).
        % To control the ratio mu_d/mu_lambda, mu_lambda needs to be adjusted, such that
        % the effective fitted mu_lambda is constant.
        if names{k} == 'p'
            mu_lambda_adjusted = mu_lambdas(i);
        elseif names{k} == 'd'
            mu_lambda_adjusted = mu_lambdas(i) / (1 + 0.435 * CV^2)^0.080;
        elseif names{k} == 'D'
            mu_lambda_adjusted = mu_lambdas(i) / (1 - 0.003 * CV + 1.045 * CV^2 - 0.113 * CV^3 + 0.0043 * CV^4)^-0.471;
        elseif strcmp(names{k}, 'all')
            mu_lambda_adjusted = mu_lambdas(i) / (1 - 0.011 * CV + 1.355 * CV^2 - 0.179 * CV^3 + 0.0077 * CV^4)^-0.357;
        end
    
        % define degradation and production rate
        mu_d = mu_D/mu_lambda_adjusted^2; % mean morphogen degradation rate [1/s]
        mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]

        fitted_lambda = NaN(nruns, 1);
        fitted_C0 = NaN(nruns, 1);

        % loop over several independent runs
        for j = 1:nruns

            % build the domain with variable cell sizes
            [l_s, l_p] = helper_functions.build_domain(LS, LP, diameter, CV_A);

            % create grid for the solver
            x0 = [-l_s, 0, l_p];
            x0 = sort([x0 x0(2:end-1)]); % duplicate cell boundary nodes

            ncS = length(l_s);
            ncP = length(l_p);
            nc = ncS + ncP;

            options = bvpset('Vectorized', 'on', 'NMax', 100*nc, 'RelTol', tol, 'AbsTol', tol);

            % default kinetic parameters
            p = mu_p * ones(nc, 1);
            d = mu_d * ones(nc, 1);
            D = mu_D * ones(nc, 1);

            % draw random kinetic parameters for each cell
            if k == 1 || k == 4
                p = random(helper_functions.logndist(mu_p, CV), nc, 1);
            end
            if k == 2 || k == 4
                d = random(helper_functions.logndist(mu_d, CV), nc, 1);
            end
            if k == 3 || k == 4
                D = random(helper_functions.logndist(mu_D, CV), nc, 1);
            end

            % get initial solution
            sol0 = bvpinit(x0, @helper_functions.y0);

            % define new function handles for boundary condition
            % function and initial condition function
            bcfun_init = @(ya, yb) helper_functions.bcfun(ya, yb, nc);

            % define function handle for the diffusion equation
            odefun_init = @(x,y,c) helper_functions.odefun(x, y, c, D, p, d, ncS);

            % solve the equation
            sol = bvp4c(odefun_init, bcfun_init, sol0, options);

            % fit an exponential in log space in the patterning domain
            idx = find(sol.x >= 0);

            param = polyfit(sol.x(idx), log(sol.y(1,idx)), 1);
            fitted_lambda(j) = -1/param(1);
            fitted_C0(j) = exp(param(2));

            % fit a hyperbolic cosine in log space in the patterning domain
            logcosh = @(p,x) p(2) + log(cosh((LP-x)/p(1)));
            mdl = fitnlm(sol.x(idx), log(sol.y(1,idx)), logcosh, [fitted_lambda(j) log(fitted_C0(j)) - log(cosh(LP/fitted_lambda(j)))], 'Options', fitopt);
            fitted_lambda(j) = mdl.Coefficients.Estimate(1);
            fitted_C0(j) = exp(mdl.Coefficients.Estimate(2)) * cosh(LP/fitted_lambda(j));


        end

        % determine the CV of the decay length and the amplitude over the independent runs + SE
        lambda(i) = mean(fitted_lambda);
        lambda_SE(i) = SEfun(fitted_lambda);
        C0(i) = mean(fitted_C0);
        C0_SE(i) = SEfun(fitted_C0);
        CV_lambda(i) = CVfun(fitted_lambda);
        CV_lambda_SE(i) = std(bootstrp(nboot, CVfun, fitted_lambda));
        CV_0(i) = CVfun(fitted_C0);
        CV_0_SE(i) = std(bootstrp(nboot, CVfun, fitted_C0));
 
    end

    writetable(table(lambda, lambda_SE, C0, C0_SE, CV_lambda, CV_lambda_SE, CV_0, CV_0_SE), filename)

end

end
