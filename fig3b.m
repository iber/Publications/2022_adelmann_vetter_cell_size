% Copyright © 2021-2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Jan Adelmann, Roman Vetter & Dagmar Iber

function fig3b

% parameters
tol = 1e-13; % numerical tolerance for solver and fitting
nruns = 1000; % number of independent simulation runs
diameter = 5; % cell diameter [µm]
mu_D = 0.033; % mean morphogen diffusion coefficient [µm^2/s]
mu_lambda = 20; % mean gradient length [µm]
mu_d = mu_D/mu_lambda^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)
ncS = 5; % number of cells in the source domain
ncP = 50; % number of cells in the patterning domain

LS = ncS * diameter; % source length
LP = ncP * diameter; % pattern length

% analytical deterministic solution
C = @(x) mu_p/mu_d * ((x<0) .* (1-cosh(x/mu_lambda)) + sinh(LS/mu_lambda) / sinh((LS+LP)/mu_lambda) * cosh((LP-x)/mu_lambda));

fitopt = statset('TolFun', tol, 'TolX', tol);

% readout concentrations
K = logspace(log10(C(0)), log10(C(LP)), 100);

% create folder to save files in if it does not already exist
dir = 'fig3b';
if not(isfolder(dir))
    mkdir(dir)
end

%% 1. noise-free case

CV_A = 0; % coefficient of variation for the cell area

filename_delta_x = [dir '/read_out_precision_centroid_delta_x_0_0.csv'];

[l_s, l_p] = helper_functions.build_domain(LS, LP, diameter, CV_A);

% create grid for the solver
x0 = [-l_s, 0, l_p];
x0 = sort([x0 x0(2:end-1)]); % duplicate cell boundary nodes

ncS = length(l_s);
ncP = length(l_p);
nc = ncS + ncP;

options = bvpset('Vectorized', 'on', 'NMax', 100*nc, 'RelTol', tol, 'AbsTol', tol);

% default: all parameters constant
p = mu_p * ones(nc, 1);
d = mu_d * ones(nc, 1);
D = mu_D * ones(nc, 1);

% get initial solution
sol0 = bvpinit(x0, @helper_functions.y0);

% define new function handles for boundary condition
% function and initial condition function
bcfun_init = @(ya, yb) helper_functions.bcfun(ya, yb, nc);

% define functioon handle for the diffusion equation
odefun_init = @(x,y,c) helper_functions.odefun(x, y, c, D, p, d, ncS);

% solve the equation
sol = bvp4c(odefun_init, bcfun_init, sol0, options);

% initialise the start location at the beginning of the patterning domain
cell_beginning = 0;

% arrays to store readout concentrations for each cell
C_average = NaN(1,ncP);
C_centroid = NaN(1,ncP);

% calculate the midpoint of each cell
x = [0, l_p(1:end-1)];
midpoint = (x + l_p) / 2;

% loop through the cells in the patterning domain
for c = 1:ncP

    % set the upper interval as the end of a cell
    cell_end = l_p(c);

    % define interval where to extract solutions
    logical_indexes = (sol.x <= cell_end) & (sol.x >= cell_beginning);

    % get length of the cell for normalisation
    cell_length = cell_end - cell_beginning;

    % get the x and y solution
    X = sol.x(logical_indexes);
    Y = sol.y(1, logical_indexes);

    % get unique x, y values for interpolation solver
    x_unique = unique(X, 'stable');
    y_unique = unique(Y, 'stable');

    % increase the resolution of points in each cell
    x_high_res = linspace(cell_beginning, cell_end, 100);

    % get interpolated solutions for better resolution
    y_high_res = pchip(x_unique, y_unique, x_high_res);

    % interpolation to find mitpoint
    C_centroid(c) = pchip(x_high_res, y_high_res, midpoint(c));

    % get the average concentration per cell
    C_average(c) = trapz(x_high_res, y_high_res) / cell_length;

    % set the lower interval for the next iteration as the
    % current end of the cell
    cell_beginning = cell_end;

end

% compute the positional shift
interp_average = interp1(C_average, midpoint, K, 'pchip', NaN);
interp_centroid = interp1(C_centroid, midpoint, K, 'pchip', NaN);
shift = interp_average - interp_centroid;

% truncate the tail where the exact solution deviates from an
% exponential due to the boundary conditions
Lmax = LP - 6*diameter;
idx_high = find(interp_average < Lmax, 1, 'last');
shift = shift(1:idx_high);

% normalise by mean gradient length
shift = shift / mu_lambda;

% calculate mean and SE
mean_shift = nanmean(shift, 'all');
SE_shift = nanstd(shift, 0, 'all') / sqrt(nnz(~isnan(shift)));

% save as csv file
writetable(table(CV_A, mu_lambda, mean_shift, SE_shift, 'VariableNames', {'CV_A', 'mu_lambda', 'delta_x', 'SE_delta_x'}), filename_delta_x);


%% noisy case

% coefficient of variation for the cell area
CV_A = [0, logspace(log10(0.01), log10(1), 19)];

% coefficient of variation for the kinetic parameters
CV = [0.3, 1];

% loop over the variability in the kinetic parameters
for i = 1:numel(CV)

    global_table = [];
    filename_delta_x = [dir '/validation_delta_x_CV_k_' num2str(CV(i)) '.csv'];

    % Variation in kinetic parameters lets the mean gradient length drift (Vetter & Iber, 2021).
    % To control the ratio mu_d/mu_lambda, mu_lambda needs to be adjusted, such that
    % the effective fitted mu_lambda is constant.
    mu_lambda_adjusted = mu_lambda / (1 - 0.011 * CV(i) + 1.355 * CV(i)^2 - 0.179 * CV(i)^3 + 0.0077 * CV(i)^4)^-0.357;

    % correction for CV = 1 is a bit too low, correct manually
    if (CV(i) == 1)
        mu_lambda_adjusted = mu_lambda_adjusted + 0.5;
    end

    % define degradation and production rate
    mu_d = mu_D/mu_lambda_adjusted^2; % mean morphogen degradation rate [1/s]
    mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)

    % loop over all CV_A variabilities
    for k = 1:numel(CV_A)

        % allocate memory
        fitted_lambda = NaN(nruns, 1);
        fitted_C0 = NaN(nruns, 1);

        % array to store cubic interpolation runs
        shift = NaN(nruns, length(K));

        for j = 1:nruns

            % build the domain with variable cell sizes
            [l_s, l_p] = helper_functions.build_domain(LS, LP, diameter, CV_A(k));

            % initialise the solver
            x0 = [-l_s, 0, l_p];
            x0 = sort([x0 x0(2:end-1)]); % duplicate cell boundary nodes

            ncS = length(l_s);
            ncP = length(l_p);
            nc = ncS + ncP;

            options = bvpset('Vectorized', 'on', 'NMax', 100*nc, 'RelTol', tol, 'AbsTol', tol);

            % draw random kinetic parameters for each cell
            p = random(helper_functions.logndist(mu_p, CV(i)), nc, 1);
            d = random(helper_functions.logndist(mu_d, CV(i)), nc, 1);
            D = random(helper_functions.logndist(mu_D, CV(i)), nc, 1);

            % get initial solution
            sol0 = bvpinit(x0, @helper_functions.y0);

            % define new function handles for boundary condition
            % function and initial condition function
            bcfun_init = @(ya, yb) helper_functions.bcfun(ya, yb, nc);

            % define functioon handle for the diffusion equation
            odefun_init = @(x,y,c) helper_functions.odefun(x, y, c, D, p, d, ncS);

            % solve the equation
            sol = bvp4c(odefun_init, bcfun_init, sol0, options);

            % fit an exponential in log space in the patterning domain
            idx = find(sol.x >= 0);

            param = polyfit(sol.x(idx), log(sol.y(1,idx)), 1);
            fitted_lambda(j) = -1/param(1);
            fitted_C0(j) = exp(param(2));

            % fit a hyperbolic cosine in log space in the patterning domain
            logcosh = @(p,x) p(2) + log(cosh((LP-x)/p(1)));
            mdl = fitnlm(sol.x(idx), log(sol.y(1,idx)), logcosh, [fitted_lambda(j) log(fitted_C0(j)) - log(cosh(LP/fitted_lambda(j)))], 'Options', fitopt);
            fitted_lambda(j) = mdl.Coefficients.Estimate(1);

            % initialise the start location at the beginning of the patterning domain
            cell_beginning = 0;

            % arrays to store readout concentrations for each cell
            C_average = NaN(1,ncP);
            C_centroid = NaN(1,ncP);

            % use the beginning of each cell as the x-coordinate
            x = [0, l_p(1:end-1)];

            % calculate the midpoint of each cell
            midpoint = (x + l_p) / 2;

            % loop through the cells in the patterning domain
            for c = 1:ncP

                % set the upper interval as the end of a cell
                cell_end = l_p(c);

                % define interval where to extract solutions
                logical_indexes = (sol.x <= cell_end) & (sol.x >= cell_beginning);

                % get length of the cell for normalisation
                cell_length = cell_end - cell_beginning;

                % get the x and y solution
                X = sol.x(logical_indexes);
                Y = sol.y(1, logical_indexes);

                % get unique x, y values for interpolation
                x_unique = unique(X, 'stable');
                y_unique = unique(Y, 'stable');

                % increase the resolution of points in each cell
                x_high_res = linspace(cell_beginning, cell_end, 100);

                % get interpolated solutions for better resolution
                y_high_res = pchip(x_unique, y_unique, x_high_res);

                % interpolation to find midpoint
                C_centroid(c) = pchip(x_high_res, y_high_res, midpoint(c));

                % get the average concentration per cell
                C_average(c) = trapz(x_high_res, y_high_res) / cell_length;

                % set the lower interval for the next iteration as the current end of the cell
                cell_beginning = cell_end;

            end

            % compute the positional shift
            interp_average = interp1(C_average, midpoint, K, 'pchip', NaN);
            interp_centroid = interp1(C_centroid, midpoint, K, 'pchip', NaN);
            shift(j, :) = interp_average - interp_centroid;

        end

        % mean gradient length
        fitted_lambda = mean(fitted_lambda);

        % normalise by mean gradient length
        shift = shift / mu_lambda;

        % calculate mean and SE
        mean_shift = nanmean(shift, 'all');
        SE_shift = nanstd(shift, 0, 'all') / sqrt(nnz(~isnan(shift)));

        % save for every CV_A value
        row = table(CV_A(k), fitted_lambda,  mean_shift, SE_shift, 'VariableNames', {'CV_A', 'mu_lambda', 'delta_x', 'SE_delta_x'});
        global_table = [global_table; row];

    end

    writetable(global_table, filename_delta_x);

end

end
