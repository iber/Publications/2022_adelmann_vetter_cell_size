% Copyright © 2021-2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Jan Adelmann, Roman Vetter & Dagmar Iber

function fig4f

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
nruns = 1000; % number of independent simulation runs
nboot = 1e4; % number of bootstrap samples for error estimation
diameter = 5; % cell diameter [µm]
mu_D = 0.033; % mean morphogen diffusion coefficient [µm^2/s]
mu_lambda = 20; % mean gradient length [µm]
mu_d = mu_D/mu_lambda^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]
ncS = 5; % number of cells in the source domain
ncP = 65; % number of cells in the patterning domain
CV = 0.3; % coefficient of variation for the kinetic parameters
CV_A = 0.5; % coefficient of variation for the cell area

LS = ncS * diameter; % source length
LP = ncP * diameter; % pattern length

% analytical deterministic solution
C = @(x) mu_p/mu_d * ((x<0) .* (1-cosh(x/mu_lambda)) + sinh(LS/mu_lambda) / sinh((LS+LP)/mu_lambda) * cosh((LP-x)/mu_lambda));

% readout concentrations at selected positions (obtained from analytical solution)
K = [C(3*mu_lambda), C(6*mu_lambda), C(12*mu_lambda)];

names = ["three_lambda", "six_lambda", "twelve_lambda"];

% define a range of varying diameters
diameters = linspace(mu_lambda/40, 2*mu_lambda, 40);

% Variation in kinetic parameters lets the mean gradient length drift (Vetter & Iber, 2021).
% To control the ratio mu_d/mu_lambda, mu_lambda needs to be adjusted, such that
% the effective fitted mu_lambda is constant.
mu_lambda_adjusted = mu_lambda / (1 - 0.011 * CV + 1.355 * CV^2 - 0.179 * CV^3 + 0.0077 * CV^4)^-0.357;

% define degradation and production rate
mu_d = mu_D/mu_lambda_adjusted^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]

% create folder to save files in if it does not already exist
dir = 'fig4f';
if not(isfolder(dir))
    mkdir(dir)
end

%% find the positional error for predefined readout positions and varying cell areas

% loop over all readout positions
for k = 1:numel(K)

    filename_centroid = [dir '/read_out_pos_' char(names(k)) '_centroid.csv'];
    filename_random = [dir '/read_out_pos_' char(names(k)) '_random.csv'];
    filename_average = [dir '/read_out_pos_' char(names(k)) '_average.csv'];

    % allocate memory to store mean readout positions
    mean_position_centroid = NaN(length(diameters)', 4);
    mean_position_random = NaN(length(diameters)', 4);
    mean_position_average = NaN(length(diameters)', 4);

    % loop over all areas
    for i = 1:numel(diameters)

        % arrays to store the x positions
        x_average = NaN(nruns, 1);
        x_centroid = NaN(nruns, 1);
        x_random = NaN(nruns, 1);

        for j = 1:nruns

            % build the domain with variable cell sizes
            [l_s, l_p] = helper_functions.build_domain(LS, LP, diameters(i), CV_A);

            % create grid for the solver
            x0 = [-l_s, 0, l_p];
            x0 = sort([x0 x0(2:end-1)]); % duplicate cell boundary nodes

            ncS = length(l_s);
            ncP = length(l_p);
            nc = ncS + ncP;
            
            options = bvpset('Vectorized', 'on', 'NMax', 100*nc, 'RelTol', tol, 'AbsTol', tol);

            % draw random kinetic parameters for each cell
            p = random(helper_functions.logndist(mu_p, CV), nc, 1);
            d = random(helper_functions.logndist(mu_d, CV), nc, 1);
            D = random(helper_functions.logndist(mu_D, CV), nc, 1);

            % get initial solution
            sol0 = bvpinit(x0, @helper_functions.y0);

            % define new function handles for boundary condition
            bcfun_init = @(ya, yb) helper_functions.bcfun(ya, yb, nc);

            % define function handle for the diffusion equation
            odefun_init = @(x,y,c) helper_functions.odefun(x, y, c, D, p, d, ncS);

            % solve the equation
            sol = bvp4c(odefun_init, bcfun_init, sol0, options);

            % initialise the start location at the beginning of the patterning domain
            cell_beginning = 0;

            % arrays to store readout concentrations for each cell
            C_average = NaN(1,ncP);
            C_random = NaN(1,ncP);
            C_centroid = NaN(1,ncP);

            % use the beginning of each cell as the x-coordinate
            cell_beginnings = [0, l_p(1:end-1)];

            % calculate the midpoint of each cell
            midpoint = (cell_beginnings + l_p) / 2;

            % loop through the cells in the patterning domain
            for c = 1:ncP

                % set the upper interval as the end of a cell
                cell_end = l_p(c);

                % define interval where to extract solutions
                logical_indexes = (sol.x <= cell_end) & (sol.x >= cell_beginning);

                % get length of the cell for normalisation
                cell_length = cell_end - cell_beginning;

                % get the x and y solution
                X = sol.x(logical_indexes);
                Y = sol.y(1, logical_indexes);

                % get unique x, y values for interpolation
                x_unique = unique(X, 'stable');
                y_unique = unique(Y, 'stable');

                % increase the resolution of points in each cell
                x_high_res = linspace(cell_beginning, cell_end, 100);

                % get interpolated solutions for better resolution
                y_high_res = pchip(x_unique, y_unique, x_high_res);

                % interpolation to find midpoint
                C_centroid(c) = pchip(x_high_res, y_high_res, midpoint(c));

                % get a random point in the cell
                rand_x = (cell_end-cell_beginning) * rand + cell_beginning;

                % get solution at that point
                C_random(c) = pchip(x_high_res, y_high_res, rand_x);

                % get the average concentration per cell
                C_average(c) = trapz(x_high_res, y_high_res) / cell_length;

                % set the lower interval for the next iteration as the
                % current end of the cell
                cell_beginning = cell_end;

            end

            % find the index where the concentration threshold is passed (beginning of a cell)
            index_average = getindex(C_average, K(k));
            temp_x_average = getcoordinate(index_average, cell_beginnings);

            % if the return array is empty, the index was out of scope,
            % meaning the concentration was attained outside of the domain
            if ~isempty(temp_x_average)
                x_average(j) = temp_x_average;
            end

            index_random = getindex(C_random, K(k));
            temp_x_random = getcoordinate(index_random, cell_beginnings);

            if ~isempty(temp_x_random)
                x_random(j) = temp_x_random;
            end

            index_centroid = getindex(C_centroid, K(k));
            temp_x_centroid = getcoordinate(index_centroid, cell_beginnings);

            if ~isempty(temp_x_centroid)
                x_centroid(j) = temp_x_centroid;
            end

        end

        % get stats
        mean_pos_random = nanmean(x_random);
        std_pos_random = nanstd(x_random);
        SE_pos_random = nanstd(bootstrp(nboot, @std, x_random)); 

        mean_pos_average = nanmean(x_average);
        std_pos_average = nanstd(x_average);
        SE_pos_average = nanstd(bootstrp(nboot, @std, x_average)); 

        mean_pos_centroid = nanmean(x_centroid);
        std_pos_centroid = nanstd(x_centroid); 
        SE_pos_centroid = nanstd(bootstrp(nboot, @std, x_centroid)); 

        % add results to arrays for each cell size
        mean_position_centroid(i, :) = [diameters(i), mean_pos_centroid, std_pos_centroid, SE_pos_centroid];
        mean_position_random(i, :) = [diameters(i), mean_pos_random, std_pos_random, SE_pos_random];
        mean_position_average(i, :) = [diameters(i), mean_pos_average, std_pos_average, SE_pos_average];

    end

    writetable(table(mean_position_centroid(:,1), mean_position_centroid(:,2), mean_position_centroid(:,3), mean_position_centroid(:,4), 'VariableNames', {'diameter', 'mean_pos_centroid', 'std_pos_centroid', 'SE_std'}), filename_centroid);
    writetable(table(mean_position_random(:,1), mean_position_random(:,2), mean_position_random(:,3), mean_position_random(:,4), 'VariableNames', {'diameter', 'mean_pos_random', 'std_pos_random', 'SE_std'}), filename_random);
    writetable(table(mean_position_average(:,1), mean_position_average(:,2), mean_position_average(:,3), mean_position_average(:,4), 'VariableNames', {'diameter', 'mean_pos_average', 'std_pos_average', 'SE_std'}),filename_average);

end


% function that returns the index where threshold concentration is reached
function index = getindex(array, k_noise)
    logical_indexes_low = array <= k_noise;
    target_index = find(logical_indexes_low, 1);
    index = target_index;
end

% returns the x value at the start of the cell where threshold is reached
function x_pos = getcoordinate(index, array)
    x_pos = array(index);
end

end
