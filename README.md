# 2022_adelmann_vetter_cell_size

J. A. Adelmann, R. Vetter, D. Iber

*The impact of cell size on morphogen gradient precision*

Development 150, dev201702 (2023)

https://doi.org/10.1242/dev.201702


## Contents

* figX.m - MATLAB script generating the data for Figure X
* helper_functions.m - MATLAB class containing code shared by all scripts
* LICENSE - License file
* revision_increased_dl.pdf - figure created in response to reviewer comment, showing independence of lambda/L_p


## Installation and system requirements

The MATLAB scripts were produced with version R2020b and require the Statistics and Machine Learning Toolbox (stats).
No further installations are required, and no further system limitations apply. 


## Usage instructions

The MATLAB scripts "figX.m" can be executed out-of-the-box by just running them in MATLAB.


## Input/Output

No manual specification of input or output is required.
The MATLAB scripts produce as output the data plotted in the paper, in the form of comma-separated value files.
Local data folders are generated automatically for each script and the data is written into these folders. 


## License

All source code is released under the 3-Clause BSD license (see LICENSE for details).
