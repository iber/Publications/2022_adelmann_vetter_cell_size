% Copyright © 2021-2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Jan Adelmann, Roman Vetter & Dagmar Iber

% helper functions to create the figures in the paper
classdef helper_functions

    methods (Static)

        %% functions for the steady-state reaction-diffusion ODE
        % D*C''(x) - p*H(x) + d*C(x) = 0

        % reaction-diffusion equation
        function dydx = odefun(x, y, c, D, p, d, ncS)
            dC = -y(2,:) / D(c); % mass flux: j = -D*grad(C)
            dj = p(c) * (c <= ncS) - d(c) * y(1,:); % conservation of mass: div(j) = p*H(-x) - d*C
            dydx = [dC; dj];
        end

        % initial guess
        function y = y0(x, c)
            y = [0; 0];
        end

        % boundary & cell interface conditions
        function res = bcfun(ya, yb, nc)
            res = ya(:);
            res(1) = ya(2, 1); % zero flux at the left end of the source domain
            res(2) = yb(2,nc); % zero flux at right end of the patterning domain

            for c = 1:nc-1
                res(2*c+1) = ya(1,c+1) - yb(1,c); % concentration continuity
                res(2*c+2) = ya(2,c+1) - yb(2,c); % flux continuity
            end
        end


        %% log-normal distribution with prescribed mean and coefficient of variation

        function pd = logndist(mu, CV)
            pd = makedist('Lognormal', 'mu', log(mu/sqrt(1+CV^2)), 'sigma', sqrt(log(1+CV^2)));
        end


        %% function to set up the spatial domain

        % builds a one-dimensional domain consisting of variably-sized cells
        function [l_s, l_p] = build_domain(LS, LP, mu_d, CV_A)

            % Inupt parameters:
            % LS =      length of source domain
            % LP =      length of patterning dmoain
            % mu_d =    mean cell diameter
            % CV_A =    coefficient of variation for cell areas

            % mean cell area
            mu_A = pi * (mu_d/2)^2 * (CV_A^2+1)^(1/4);

            % Build the source domain
            l_s = [];
            while sum(l_s) < LS
                A = random(helper_functions.logndist(mu_A, CV_A), 1, 1);
                diam = 2*sqrt(A/pi);
                l_s = [l_s, diam];
            end

            % remove the last cell if the domain length is closer to the target without it
            if length(l_s) > 1 && abs(sum(l_s) - LS) >= abs(sum(l_s) - l_s(end) - LS)
                l_s = l_s(1:end-1);
            end

            % calculate the normalised diameter for each cell
            l_s = fliplr(cumsum(l_s / sum(l_s) * LS));

            % Build the patterning domain
            l_p = [];
            while sum(l_p) < LP
                A = random(helper_functions.logndist(mu_A, CV_A), 1, 1);
                diam = 2*sqrt(A/pi);
                l_p = [l_p, diam];
            end

            % remove the last cell if the domain length is closer to the target without it
            if length(l_p) > 1 && abs(sum(l_p) - LP) >= abs(sum(l_p) - l_p(end) - LP)
                l_p = l_p(1:end-1);
            end

            % calculate the normalised diameter for each cell
            l_p = cumsum(l_p / sum(l_p) * LP);

        end

    end
end
