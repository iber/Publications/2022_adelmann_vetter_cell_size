% Copyright © 2021-2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Jan Adelmann, Roman Vetter & Dagmar Iber

function fig4c

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
nruns = 1000; % number of independent simulation runs
nboot = 1e4; % number of bootstrap samples for error estimation
diameter = 5; % cell diameter [µm]
mu_D = 0.033; % mean morphogen diffusion coefficient [µm^2/s]
mu_lambda = 20; % mean gradient length [µm]
ncS = 5; % number of cells in the source domain
ncP = 65; % number of cells in the patterning domain
CV = 0.3; % coefficient of variation for the kinetic parameters
CV_A = 0.5; % coefficient of variation for the cell area

LS = ncS * diameter; % source length
LP = ncP * diameter; % pattern length

CVfun = @(x) nanstd(x) ./ nanmean(x);
SEfun = @(x) nanstd(x) ./ sqrt(sum(~isnan(x)));

cilium_lengths = [linspace(5,25,10), linspace(27.5,50,10)];

% readout positions
readout_pos = [3*mu_lambda, 6*mu_lambda, 9*mu_lambda];
names = ["three_lambda", "six_lambda", "nine_lambda"];

% Variation in kinetic parameters lets the mean gradient length drift (Vetter & Iber, 2021).
% To control the ratio mu_d/mu_lambda, mu_lambda needs to be adjusted, such that
% the effective fitted mu_lambda is constant.
mu_lambda_adjusted = mu_lambda / (1 - 0.011 * CV + 1.355 * CV^2 - 0.179 * CV^3 + 0.0077 * CV^4)^-0.357;

% define degradation and production rate
mu_d = mu_D/mu_lambda_adjusted^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)

% create folder to save files in if it does not already exist
dir = 'fig4c';
if not(isfolder(dir))
    mkdir(dir)
end

%% effect of averaging over the cilium on the variability in the concentrations

% loop over the readout positions
for pos = 1:numel(readout_pos)

    % set up table to store the results in
    global_table = cell2table(cell(0,6),'VariableNames', {'mean_diameter','Cilium_length', 'conc_cilium','conc_SE_cilium', 'conc_CV_cilium', 'conc_CV_SE_ciliuum'});

    % set up the file name + path
    filename_cilium = [dir '/read_out_precision_cilium_conc_' char(names(pos)) '.csv'];

    % loop over the different cilium lengths
    for l = cilium_lengths

        % store the mean cell diameter
        mean_cell_diam = NaN(nruns, 1);

        % loop over several independent runs
        C_cilium = NaN(nruns, 1);

        for j = 1:nruns

            % build the domain with variable cell sizes
            [l_s, l_p] = helper_functions.build_domain(LS, LP, diameter, CV_A);

            % create grid for the solver
            x0 = [-l_s, 0, l_p];
            x0 = sort([x0 x0(2:end-1)]); % duplicate cell boundary nodes

            ncS = length(l_s);
            ncP = length(l_p);
            nc = ncS + ncP;
            
            options = bvpset('Vectorized', 'on', 'NMax', 100*nc, 'RelTol', tol, 'AbsTol', tol);

            % draw random kinetic parameters for each cell
            p = random(helper_functions.logndist(mu_p, CV), nc, 1);
            d = random(helper_functions.logndist(mu_d, CV), nc, 1);
            D = random(helper_functions.logndist(mu_D, CV), nc, 1);

            % get initial solution
            sol0 = bvpinit(x0, @helper_functions.y0);

            % define new function handles for boundary condition
            % function and initial condition function
            bcfun_init = @(ya, yb) helper_functions.bcfun(ya, yb, nc);

            % define function handle for the diffusion equation
            odefun_init = @(x,y,c) helper_functions.odefun(x, y, c, D, p, d, ncS);

            % solve the equation
            sol = bvp4c(odefun_init, bcfun_init, sol0, options);

            % average cell diam
            mean_cell_diam(j) = mean(diff(l_p));

            % calculate the midpoint of each cell
            x = [0, l_p(1:end-1)];
            midpoint = (x + l_p) / 2;

            index = (x <= readout_pos(pos));

            middle_of_cell = max(midpoint(index));

            % define interval where to extract solutions
            logical_indexes = (sol.x >= middle_of_cell - l) & (sol.x <= middle_of_cell + l);

            % get the x and y solution
            X = sol.x(logical_indexes);
            Y = sol.y(1, logical_indexes);

            % get unique x, y values for interpolation
            x_unique = unique(X, 'stable');
            y_unique = unique(Y, 'stable');

            % increase the resolution of points in each cilium
            x_high_res = linspace(middle_of_cell - l, middle_of_cell + l, 100);

            % get interpolated solutions for better resolution
            y_high_res = pchip(x_unique, y_unique, x_high_res);

            % get the average concentration in the cilium range
            C_cilium(j, :) = trapz(x_high_res, y_high_res) / (2*l);

        end

        % calculate the mean cell diameter for the n runs
        mean_diameter = nanmean(mean_cell_diam);

        % Caculate summary statistics for the nruns
        % calculate the mean concentration at each position
        conc_cilium = mean(C_cilium);

        % calculate the standard error at each position
        conc_SE_cilium = SEfun(C_cilium);

        % calculate the coefficient of variation at each position
        conc_CV_cilium = CVfun(C_cilium);

        % calculate the SE for the coefficient of variation
        conc_CV_SE_cilium = std(bootstrp(nboot, CVfun, C_cilium));

        % define a table for each iteration
        row = table(mean_diameter, l / mean_diameter, conc_cilium, conc_SE_cilium, conc_CV_cilium, conc_CV_SE_cilium, 'VariableNames', {'mean_diameter','cilium_length', 'conc_cilium','conc_SE_cilium', 'conc_CV_cilium', 'conc_CV_SE_ciliuum'});
        global_table = [global_table; row];

    end
    
    writetable(global_table, filename_cilium);

end

end
