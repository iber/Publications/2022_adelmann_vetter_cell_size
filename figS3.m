% Copyright © 2021-2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Jan Adelmann, Roman Vetter & Dagmar Iber

function figS3

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
nruns = 1000; % number of independent simulation runs
nboot = 1e4; % number of bootstrap samples for error estimation
diameter = 5; % cell diameter [µm]
mu_D = 0.033; % mean morphogen diffusion coefficient [µm^2/s]
mu_lambda = 20; % mean gradient length [µm]
mu_d = mu_D/mu_lambda^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]
ncS = 5; % number of cells in the source domain
ncP = 65; % number of cells in the ßpatterning domain
CV = 0.3; % coefficient of variation for the kinetic parameters
CV_A = 0.5; % coefficient of variation for the cell area

LS = ncS * diameter; % source length
LP = ncP * diameter; % pattern length

% analytical deterministic solution
C = @(x) mu_p/mu_d * ((x<0) .* (1-cosh(x/mu_lambda)) + sinh(LS/mu_lambda) / sinh((LS+LP)/mu_lambda) * cosh((LP-x)/mu_lambda));

% readout concentrations at selected positions (obtained from analytical solution)
K = [C(3*mu_lambda), C(6*mu_lambda), C(12*mu_lambda)];

names = ["three_lambda", "six_lambda", "twelve_lambda"];

% define a range of varying diameters
diameters = linspace(mu_lambda/10, 2*mu_lambda, 40);

% Variation in kinetic parameters lets the mean gradient length drift (Vetter & Iber, 2022).
% To control the ratio mu_d/mu_lambda, mu_lambda needs to be adjusted, such that
% the effective fitted mu_lambda is constant.
mu_lambda_adjusted = mu_lambda / (1 - 0.011 * CV + 1.355 * CV^2 - 0.179 * CV^3 + 0.0077 * CV^4)^-0.357;

% define degradation and production rate
mu_d = mu_D/mu_lambda_adjusted^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]

% create folder to save files
dir = 'correlation_length';
if not(isfolder(dir))
    mkdir(dir)
end

%% find the positional error for predefined readout positions and varying cell areas

% loop over all readout positions
for k = 1:numel(K)

    filename_centroid_ascending = [dir '/read_out_pos_' char(names(k)) '_centroid_ascending_params.csv'];
    filename_centroid_descending = [dir '/read_out_pos_' char(names(k)) '_centroid_descending_params.csv'];
    filename_centroid_constant = [dir '/read_out_pos_' char(names(k)) '_centroid_constant_params.csv'];
    filename_random_ascending = [dir '/read_out_pos_' char(names(k)) '_random_ascending_params.csv'];
    filename_random_descending = [dir '/read_out_pos_' char(names(k)) '_random_descending_params.csv'];
    filename_random_constant = [dir '/read_out_pos_' char(names(k)) '_random_constant_params.csv'];
    filename_average_ascending = [dir '/read_out_pos_' char(names(k)) '_average_ascending_params.csv'];
    filename_average_descending = [dir '/read_out_pos_' char(names(k)) '_average_descending_params.csv'];
    filename_average_constant = [dir '/read_out_pos_' char(names(k)) '_average_constant_params.csv'];

    % allocate memory to store mean readout positions
    mean_position_centroid_ascending = NaN(length(diameters)', 4);
    mean_position_centroid_descending = NaN(length(diameters)', 4);
    mean_position_centroid_constant = NaN(length(diameters)', 4);
    mean_position_random_ascending = NaN(length(diameters)', 4);
    mean_position_random_descending = NaN(length(diameters)', 4);
    mean_position_random_constant= NaN(length(diameters)', 4);
    mean_position_average_ascending = NaN(length(diameters)', 4);
    mean_position_average_descending = NaN(length(diameters)', 4);
    mean_position_average_constant = NaN(length(diameters)', 4);

    % loop over all areas
    for i = 1:numel(diameters)

        % arrays to store the x positions
        x_average_ascending_params = NaN(nruns, 1);
        x_average_descending_params = NaN(nruns, 1);
        x_average_constant_params = NaN(nruns, 1);
        x_centroid_ascending_params = NaN(nruns, 1);
        x_centroid_descending_params = NaN(nruns, 1);
        x_centroid_constant_params = NaN(nruns, 1);
        x_random_ascending_params = NaN(nruns, 1);
        x_random_descending_params = NaN(nruns, 1);
        x_random_constant_params = NaN(nruns, 1);

        for j = 1:nruns

            % build the domain with variable cell sizes
            [l_s, l_p] = helper_functions.build_domain(LS, LP, diameters(i), CV_A);

            % create grid for the solver
            x0 = [-l_s, 0, l_p];
            x0 = sort([x0 x0(2:end-1)]); % duplicate cell boundary nodes

            ncS = length(l_s);
            ncP = length(l_p);
            nc = ncS + ncP;
            
            options = bvpset('Vectorized', 'on', 'NMax', 100*nc, 'RelTol', tol, 'AbsTol', tol);
            
            % ===================== %
            % Complete Correleation %
            % ===================== %

            % draw one random kinetic parameter
            p_single = random(helper_functions.logndist(mu_p, CV), 1, 1);
            d_single = random(helper_functions.logndist(mu_d, CV), 1, 1);
            D_single = random(helper_functions.logndist(mu_D, CV), 1, 1);
            
            % add the parameter to each cell            
            p_constant = NaN(nc, 1);
            d_constant = NaN(nc, 1);
            D_constant = NaN(nc, 1);
            
            p_constant(:) = p_single;
            d_constant(:) = d_single;
            D_constant(:) = D_single;

            % ===================== %
            % Partial Correleation  %
            % ===================== %
            
            % draw random kinetic parameters for each cell
            p = random(helper_functions.logndist(mu_p, CV), nc, 1);
            d = random(helper_functions.logndist(mu_d, CV), nc, 1);
            D = random(helper_functions.logndist(mu_D, CV), nc, 1);
            
            % sort the kinetic parameters 
            p_ascending = sort(p);
            d_ascending = sort(d);
            D_ascending = sort(D);
            
            p_descending = sort(p, 'descend');
            d_descending = sort(d, 'descend');
            D_descending = sort(D, 'descend');

            % get initial solution
            sol0 = bvpinit(x0, @helper_functions.y0);

            % define new function handles for boundary condition
            bcfun_init = @(ya, yb) helper_functions.bcfun(ya, yb, nc);

            % define function handle for the diffusion equation
            odefun_init_ascending_params = @(x,y,c) helper_functions.odefun(x, y, c, D_ascending, p_ascending, d_ascending, ncS);
            odefun_init_descending_params = @(x,y,c) helper_functions.odefun(x, y, c, D_descending, p_descending, d_descending, ncS);
            odefun_init_constant_params = @(x,y,c) helper_functions.odefun(x, y, c, D_constant, p_constant, d_constant, ncS);
            
            % solve the equation
            sol_ascending_params = bvp4c(odefun_init_ascending_params, bcfun_init, sol0, options);
            sol_descending_params = bvp4c(odefun_init_descending_params, bcfun_init, sol0, options);
            sol_constant_params = bvp4c(odefun_init_constant_params, bcfun_init, sol0, options);
                        
            % use the beginning of each cell as the x-coordinate
            cell_beginnings = [0, l_p(1:end-1)];
             
            [average_C_ascending_params, centroid_C_ascending_params, random_C_ascending_params] = conc_per_cell(sol_ascending_params, l_p, ncP, cell_beginnings);
            [average_C_descending_params, centroid_C_descending_params, random_C_descending_params] = conc_per_cell(sol_descending_params, l_p, ncP, cell_beginnings);
            [average_C_constant_params, centroid_C_constant_params, random_C_constant_params] = conc_per_cell(sol_constant_params, l_p, ncP, cell_beginnings);
        
            % ================================================== %
            % Average Method/ (only this method is plotted in the
            % supplement)
            % ================================================== %

            % find the index where the concentration threshold is passed. 
            index_average_ascending_params = getindex(average_C_ascending_params, K(k));
            temp_x_average_ascending_params = getcoordinate(index_average_ascending_params, cell_beginnings);
            
            index_average_descending_params = getindex(average_C_descending_params, K(k));
            temp_x_average_descending_params = getcoordinate(index_average_descending_params, cell_beginnings);
            
            index_average_constant_params = getindex(average_C_constant_params, K(k));
            temp_x_average_constant_params = getcoordinate(index_average_constant_params, cell_beginnings);

            % if the return array is empty, the index was out of scope,
            % meaning the concentration was attained outside of the
            % domain
            if ~isempty(temp_x_average_ascending_params)
                x_average_ascending_params(j) = temp_x_average_ascending_params;
            end
            if ~isempty(temp_x_average_descending_params)
                x_average_descending_params(j) = temp_x_average_descending_params;
            end
          
            if ~isempty(temp_x_average_constant_params)
                x_average_constant_params(j) = temp_x_average_constant_params;
            end

            % ================================================== %
            % Random Method
            % ================================================== %
          
            index_random_ascending_params = getindex(random_C_ascending_params, K(k));
            temp_x_random_ascending_params = getcoordinate(index_random_ascending_params, cell_beginnings);
            
            index_random_descending_params = getindex(random_C_descending_params, K(k));
            temp_x_random_descending_params = getcoordinate(index_random_descending_params, cell_beginnings);
            
            index_random_constant_params = getindex(random_C_constant_params, K(k));
            temp_x_random_constant_params = getcoordinate(index_random_constant_params, cell_beginnings);

            if ~isempty(temp_x_random_ascending_params)
                x_random_ascending_params(j) = temp_x_random_ascending_params;
            end
            
            if ~isempty(temp_x_random_descending_params)
                x_random_descending_params(j) = temp_x_random_descending_params;
            end
         
            if ~isempty(temp_x_random_constant_params)
                x_random_constant_params(j) = temp_x_random_constant_params;
            end

            % ================================================== %
            % Centroid Method
            % ================================================== %
           
            index_centroid_ascending_params = getindex(centroid_C_ascending_params, K(k));
            temp_x_centroid_ascending_params = getcoordinate(index_centroid_ascending_params, cell_beginnings);
            
            index_centroid_descending_params = getindex(centroid_C_descending_params, K(k));
            temp_x_centroid_decscending_params = getcoordinate(index_centroid_descending_params, cell_beginnings);
            
            index_centroid_constant_params = getindex(centroid_C_constant_params, K(k));
            temp_x_centroid_constant_params = getcoordinate(index_centroid_constant_params, cell_beginnings);

            if ~isempty(temp_x_centroid_ascending_params)
                x_centroid_ascending_params(j) = temp_x_centroid_ascending_params;
            end
            
            if ~isempty(temp_x_centroid_decscending_params)
                x_centroid_descending_params(j) = temp_x_centroid_decscending_params;
            end
          
            if ~isempty(temp_x_centroid_constant_params)
                x_centroid_constant_params(j) = temp_x_centroid_constant_params;
            end
          
        end

        [mean_pos_average_ascending, std_pos_average_ascending, SE_pos_average_ascending] =  get_stats(x_average_ascending_params);
        [mean_pos_average_descending, std_pos_average_descending, SE_pos_average_descending] =  get_stats(x_average_descending_params);
        [mean_pos_average_constant, std_pos_average_constant, SE_pos_average_constant] =  get_stats(x_average_constant_params);
 
        mean_position_average_ascending(i, :) = [diameters(i), mean_pos_average_ascending, std_pos_average_ascending, SE_pos_average_ascending];
        mean_position_average_descending(i, :) = [diameters(i), mean_pos_average_descending, std_pos_average_descending, SE_pos_average_descending];
        mean_position_average_constant(i, :) = [diameters(i), mean_pos_average_constant, std_pos_average_constant, SE_pos_average_constant];
    
        [mean_pos_centroid_ascending, std_pos_centroid_ascending, SE_pos_centroid_ascending] =  get_stats(x_centroid_ascending_params);
        [mean_pos_centroid_descending, std_pos_centroid_descending, SE_pos_centroid_descending] =  get_stats(x_centroid_descending_params);
        [mean_pos_centroid_constant, std_pos_centroid_constant, SE_pos_centroid_constant] =  get_stats(x_centroid_constant_params);
 
        mean_position_centroid_ascending(i, :) = [diameters(i) , mean_pos_centroid_ascending, std_pos_centroid_ascending, SE_pos_centroid_ascending];
        mean_position_centroid_descending(i, :) = [diameters(i) , mean_pos_centroid_descending, std_pos_centroid_descending, SE_pos_centroid_descending];
        mean_position_centroid_constant(i, :) = [diameters(i) , mean_pos_centroid_constant, std_pos_centroid_constant, SE_pos_centroid_constant];
        
        [mean_pos_random_ascending, std_pos_random_ascending, SE_pos_random_ascending] =  get_stats(x_random_ascending_params);
        [mean_pos_random_descending, std_pos_random_descending, SE_pos_random_descending] =  get_stats(x_random_descending_params);
        [mean_pos_random_constant, std_pos_random_constant, SE_pos_random_constant] =  get_stats(x_random_constant_params);
 
        mean_position_random_ascending(i, :) = [diameters(i) , mean_pos_random_ascending, std_pos_random_ascending, SE_pos_random_ascending];
        mean_position_random_descending(i, :) = [diameters(i) , mean_pos_random_descending, std_pos_random_descending, SE_pos_random_descending];
        mean_position_random_constant(i, :) = [diameters(i) , mean_pos_random_constant, std_pos_random_constant, SE_pos_random_constant];

    
    end
    
    write_to_file(filename_average_ascending, mean_position_average_ascending);
    write_to_file(filename_average_descending, mean_position_average_descending);
    write_to_file(filename_average_constant, mean_position_average_constant);
    
    write_to_file(filename_centroid_ascending, mean_position_centroid_ascending);
    write_to_file(filename_centroid_descending, mean_position_centroid_descending);
    write_to_file(filename_centroid_constant, mean_position_centroid_constant);
    
    write_to_file(filename_random_ascending, mean_position_random_ascending);
    write_to_file(filename_random_descending, mean_position_random_descending);
    write_to_file(filename_random_constant, mean_position_random_constant);
  
end


% function that returns the index where threshold concentration is reached
function index = getindex(array, k_noise)
    logical_indexes_low = array <= k_noise;
    target_index = find(logical_indexes_low, 1);
    index = target_index;
end

% returns the x value at the start of the cell where threshold is reached
function x_pos = getcoordinate(index, array)
    x_pos = array(index);
end

function [C_average, C_centroid, C_random] = conc_per_cell(sol, l_p, ncP, cell_beginnings)
    
    % initialise the start location at the beginning of the patterning domain
    cell_beginning = 0;

    % arrays to store readout concentrations for each cell
    C_average = NaN(1,ncP);
    C_random = NaN(1,ncP);
    C_centroid = NaN(1,ncP);

    % calculate the midpoint of each cell
    midpoint = (cell_beginnings + l_p) / 2;

    % loop through the cells in the patterning domain
    for c = 1:ncP

        % set the upper interval as the end of a cell
        cell_end = l_p(c);

        % define interval where to extract solutions
        logical_indexes = (sol.x <= cell_end) & (sol.x >= cell_beginning);

        % get length of the cell for normalisation
        cell_length = cell_end - cell_beginning;

        % get the x and y solution
        X = sol.x(logical_indexes);
        Y = sol.y(1, logical_indexes);

        % get unique x, y values for interpolation
        x_unique = unique(X, 'stable');
        y_unique = unique(Y, 'stable');

        % increase the resolution of points in each cell
        x_high_res = linspace(cell_beginning, cell_end, 100);

        % get interpolated solutions for better resolution
        y_high_res = pchip(x_unique, y_unique, x_high_res);

        % interpolation to find midpoint
        C_centroid(c) = pchip(x_high_res, y_high_res, midpoint(c));

        % get a random point in the cell
        rand_x = (cell_end-cell_beginning) * rand + cell_beginning;

        % get solution at that point
        C_random(c) = pchip(x_high_res, y_high_res, rand_x);

        % get the average concentration per cell
        C_average(c) = trapz(x_high_res, y_high_res) / cell_length;

        % set the lower interval for the next iteration as the
        % current end of the cell
        cell_beginning = cell_end;

    end
end

function [mean_pos, std_pos, SE_pos] = get_stats(x)
     
    % get stats
    mean_pos= nanmean(x);
    std_pos= nanstd(x);
    SE_pos = nanstd(bootstrp(nboot, @nanstd, x));
    
end    

function write_to_file(filename, data)
    
       writetable(table(data(:,1), data(:,2), data(:,3), data(:,4), 'VariableNames', {'diameter', 'mean_pos', 'std_pos', 'SE_std'}), filename);
     
end

end
