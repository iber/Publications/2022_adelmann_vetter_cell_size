% Copyright © 2021-2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Jan Adelmann, Roman Vetter & Dagmar Iber

function figS6

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
nruns = 1000; % number of independent simulation runs
nboot = 1e4; % number of bootstrap samples for error estimation
mu_lambda = 20; % mean gradient length [µm]
mu_D = 0.033; % mean morphogen diffusion coefficient [µm^2/s]
ncP = 50; % number of cells in the patterning domain
CV = 0.3; % coefficient of variation for the kinetic parameters
CV_A = 0.5; % coefficient of variation for the cell area 
num_cells_ls = [5,20]; % number of cells in the source 
diameter = 5; %[µm]
LP = ncP * diameter; % patterning domain length (constant) 
readout_positions = [3*20, 6*20]; % multiples of lambda

% uncomment one of the three options to get the different outputs in of
% fig. 

% Constant diameter of cells in the source , variable diameters in
% patterning domain

diam_lp = linspace(mu_lambda/40, 2*mu_lambda, 30);
diam_ls = 5*ones(length(diam_lp), 1);
name_output = 'varying_lp';
const_diam = 'five_mu';

% Constant diameter of cells in the patterning domain , variable diameters in
% the source 

%diam_ls = linspace(mu_lambda/40, 2*mu_lambda, 30); % cell diameter in the source [µm]
%diam_lp = 5*ones(length(diam_ls), 1); % cell diameter in the patterning domain [µm]
%name_output = 'varying_ls';
%const_diam = 'five_mu';

% both diameters vary 

%diam_ls = linspace(mu_lambda/40, 2*mu_lambda, 30); % cell diameter in the source [µm]
%diam_lp = diam_ls; % cell diameter in the patterning domain [µm]
%name_output = 'both_varying';
%const_diam = 'none';

% Variation in kinetic parameters lets the mean gradient length drift (Vetter & Iber, 2021).
% To control the ratio mu_d/mu_lambda, mu_lambda needs to be adjusted, such that
% the effective fitted mu_lambda is constant.
mu_lambda_adjusted = mu_lambda / (1 - 0.011 * CV + 1.355 * CV^2 - 0.179 * CV^3 + 0.0077 * CV^4)^-0.357;

% define degradation and production rate
mu_d = mu_D/mu_lambda_adjusted^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]

% create folder to save files in if it does not already exist
dir = 'figS6';
if not(isfolder(dir))
    mkdir(dir)
end


for i = 1:length(num_cells_ls)

    filename_average = [dir '/ls_cell_num_' num2str(num_cells_ls(i)) '_average_mu_lambda_' name_output '_diam_const_' const_diam '.csv'];
    filename_centroid = [dir '/ls_cell_num_' num2str(num_cells_ls(i)) '_centroid_mu_lambda_' name_output '_diam_const_' const_diam '.csv'];
    filename_random = [dir '/ls_cell_num_' num2str(num_cells_ls(i)) '_random_mu_lambda_' name_output '_diam_const_' const_diam '.csv'];

    mean_position_centroid = NaN(length(readout_positions)*length(diam_ls), 7);
    mean_position_random = NaN(length(readout_positions)*length(diam_ls), 7);
    mean_position_average = NaN(length(readout_positions)*length(diam_ls), 7);
       
    for k = 1:length(diam_ls)

        diameter_runs_ls = NaN(nruns, 1);
        diameter_runs_lp = NaN(nruns, 1);

        LS = num_cells_ls(i) * diam_ls(k);  % source length 

        % analytical deterministic solution
        C = @(x, LS) mu_p/mu_d * ((x<0) .* (1-cosh(x/mu_lambda)) + sinh(LS/mu_lambda) / sinh((LS+LP)/mu_lambda) * cosh((LP-x)/mu_lambda));
        
        % readout concentrations at selected positions (obtained from analytical solution)
        K = C(readout_positions, LS);

        % arrays to store the x positions
        x_average = NaN(nruns, length(K));
        x_centroid = NaN(nruns, length(K));
        x_random = NaN(nruns, length(K));

        for j = 1:nruns
        
            % build the domain with variable cell sizes
            [l_s, l_p] = build_domain(LS, LP, diam_lp(k), diam_ls(k), CV_A, num_cells_ls(i));
        
            % create grid for the solver
            x0 = [-l_s, 0, l_p];
            x0 = sort([x0 x0(2:end-1)]); % duplicate cell boundary nodes
        
            ncS = length(l_s);
            ncP = length(l_p);
            nc = ncS + ncP;
            
            options = bvpset('Vectorized', 'on', 'NMax', 100*nc, 'RelTol', tol, 'AbsTol', tol);
        
            % draw random kinetic parameters for each cell
            p = random(helper_functions.logndist(mu_p, CV), nc, 1);
            d = random(helper_functions.logndist(mu_d, CV), nc, 1);
            D = random(helper_functions.logndist(mu_D, CV), nc, 1);
        
            % get initial solution
            sol0 = bvpinit(x0, @helper_functions.y0);
        
            % define new function handles for boundary condition
            bcfun_init = @(ya, yb) helper_functions.bcfun(ya, yb, nc);
        
            % define function handle for the diffusion equation
            odefun_init = @(x,y,c) helper_functions.odefun(x, y, c, D, p, d, ncS);
        
            % solve the equation
            sol = bvp4c(odefun_init, bcfun_init, sol0, options);
        
            % arrays to store readout concentrations for each cell
            C_average = NaN(1,nc);
            C_random = NaN(1,nc);
            C_centroid = NaN(1,nc);
        
            % get cell boundaries 
            cell_ends = [-l_s(2:end),0,l_p];
            cell_beginnings = [-l_s,0,l_p(1:end-1)];
        
            % calculate the midpoint of each cell
            midpoint = (cell_beginnings + cell_ends) / 2;
        
            % loop through the cells in the patterning domain
            for c = 1:nc
        
                % get the cell beginning 
                cell_beginning = cell_beginnings(c);
        
                % set the upper interval as the end of a cell
                cell_end = cell_ends(c);
        
                % define interval where to extract solutions
                logical_indexes = (sol.x <= cell_end) & (sol.x >= cell_beginning);
        
                % get length of the cell for normalisation
                cell_length = cell_end - cell_beginning;
        
                % get the x and y solution
                X = sol.x(logical_indexes);
                Y = sol.y(1, logical_indexes);
        
                % get unique x, y values for interpolation
                x_unique = unique(X, 'stable');
                y_unique = unique(Y, 'stable');
        
                % increase the resolution of points in each cell
                x_high_res = linspace(cell_beginning, cell_end, 100);
        
                % get interpolated solutions for better resolution
                y_high_res = pchip(x_unique, y_unique, x_high_res);
        
                % interpolation to find midpoint
                C_centroid(c) = pchip(x_high_res, y_high_res, midpoint(c));
        
                % get a random point in the cell
                rand_x = (cell_end-cell_beginning) * rand + cell_beginning;
        
                % get solution at that point
                C_random(c) = pchip(x_high_res, y_high_res, rand_x);
        
                % get the average concentration per cell
                C_average(c) = trapz(x_high_res, y_high_res) / cell_length;
        
            end
        
            % find the index where the concentration threshold is passed. (Beginning of a cell)
            x_average(j,:) = getindex(C_average, K, cell_beginnings);
            x_random(j,:) = getindex(C_random, K, cell_beginnings);
            x_centroid(j,:) = getindex(C_centroid, K, cell_beginnings);
    

            diameter_runs_ls(j) = mean(diff(-l_s));
            diameter_runs_lp(j) = mean(diff(l_p));

        end 

        % set all negative values to NaN (they lie outside the patterning domain) &
        x_average(x_average<0) = NaN;
        x_random(x_random<0) = NaN;
        x_centroid(x_centroid<0) = NaN;
        
        % get stats

        mean_pos_random = nanmean(x_random);
        std_pos_random = nanstd(x_random);
        SE_pos_random = nanstd(bootstrp(nboot, @std, x_random)); 
        
        mean_pos_average = nanmean(x_average);
        std_pos_average = nanstd(x_average);
        SE_pos_average = nanstd(bootstrp(nboot, @std, x_average)); 
        
        mean_pos_centroid = nanmean(x_centroid);
        std_pos_centroid = nanstd(x_centroid); 
        SE_pos_centroid = nanstd(bootstrp(nboot, @std, x_centroid)); 

        diameter_run_ls = mean(diameter_runs_ls);
        diameter_run_lp = mean(diameter_runs_lp);
        
        % readout 3*lambda
        mean_position_centroid(k, :) = [readout_positions(1), num_cells_ls(i), diameter_run_ls, diameter_run_lp, mean_pos_centroid(1), std_pos_centroid(1), SE_pos_centroid(1)];
        mean_position_random(k, :) = [readout_positions(1), num_cells_ls(i), diameter_run_ls, diameter_run_lp, mean_pos_random(1), std_pos_random(1), SE_pos_random(1)];
        mean_position_average(k, :) = [readout_positions(1), num_cells_ls(i), diameter_run_ls, diameter_run_lp, mean_pos_average(1), std_pos_average(1), SE_pos_average(1)];
        
        % readout 6*lambda 
        mean_position_centroid(k+length(diam_ls), :) = [readout_positions(2), num_cells_ls(i), diameter_run_ls, diameter_run_lp, mean_pos_centroid(2), std_pos_centroid(2), SE_pos_centroid(2)];
        mean_position_random(k+length(diam_ls), :) = [readout_positions(2), num_cells_ls(i), diameter_run_ls, diameter_run_lp, mean_pos_random(2), std_pos_random(2), SE_pos_random(2)];
        mean_position_average(k+length(diam_ls), :) = [readout_positions(2), num_cells_ls(i), diameter_run_ls, diameter_run_lp, mean_pos_average(2), std_pos_average(2), SE_pos_average(2)];

 
    end 
    
     T_average = array2table(mean_position_average, 'VariableNames', {'readout_position', 'num_cells_ls', 'diameter_ls', 'diameter_lp', 'mean_pos_average', 'std_pos_average', 'SE_std'});
     writetable(T_average, filename_average);

     T_centroid = array2table(mean_position_centroid, 'VariableNames', {'readout_position', 'num_cells_ls', 'diameter_ls', 'diameter_lp', 'mean_pos_centroid', 'std_pos_centroid', 'SE_std'});
     writetable(T_centroid, filename_centroid);

     T_random = array2table(mean_position_centroid, 'VariableNames', {'readout_position', 'num_cells_ls', 'diameter_ls', 'diameter_lp', 'mean_pos_random', 'std_pos_random', 'SE_std'});
     writetable(T_random, filename_random);


end 

function pos = getindex(array, K, domain)

    % allocate memory 
    index = NaN(1, length(K));
    pos = NaN(1, length(K));

    % loop over concentrations and retrieve x position 
    for conc = 1:length(K)   
        if ~isnan(find(array <= K(conc)))
            index(1, conc) = find((array <= K(conc)), 1);
            pos(1, conc) = domain(index(1, conc));            
        end
    end
    
end

 % builds a one-dimensional domain consisting of variably-sized cells
function [l_s, l_p] = build_domain(LS, LP, mu_d, mu_d_ls,  CV_A, cells_source)

    % Inupt parameters:
    % LS =      length of source domain
    % LP =      length of patterning dmoain
    % mu_d =    mean cell diameter
    % CV_A =    coefficient of variation for cell areas

    % mean cell area
    mu_A = pi * (mu_d/2)^2 * (CV_A^2+1)^(1/4);
    mu_A_ls=  pi * (mu_d_ls/2)^2 * (CV_A^2+1)^(1/4);
    
    % Build the source domain
    l_s = [];

    for cell_ls = 1:cells_source
        A = random(helper_functions.logndist(mu_A_ls, CV_A), 1, 1);
        diam_ = 2*sqrt(A/pi);
        l_s = [l_s, diam_];
    end

    % calculate the normalised diameter for each cell
    l_s = fliplr(cumsum(l_s / sum(l_s) * LS));

    % Build the patterning domain 
    l_p = [];

    while sum(l_p) < LP
        A = random(helper_functions.logndist(mu_A, CV_A), 1, 1);
        diam_ = 2*sqrt(A/pi);
        l_p = [l_p, diam_];
    end

    % remove the last cell if the domain length is closer to the target without it
    if length(l_p) > 1 && abs(sum(l_p) - LP) >= abs(sum(l_p) - l_p(end) - LP)
        l_p = l_p(1:end-1);
    end

    % calculate the normalised diameter for each cell
    l_p = cumsum(l_p / sum(l_p) * LP);

end

end
