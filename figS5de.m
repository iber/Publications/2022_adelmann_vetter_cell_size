% Copyright © 2021-2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Jan Adelmann, Roman Vetter & Dagmar Iber

function figS5de

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
nruns = 1000; % number of independent simulation runs
nboot = 1e4; % number of bootstrap samples for error estimation
diameter = 5; % cell diameter [µm]
mu_lambda = 20; % mean gradient length [µm]
mu_D = 0.033; % mean morphogen diffusion coefficient [µm^2/s]
ncP = 50; % number of cells in the patterning domain
CV = 0.3; % coefficient of variation for the kinetic parameters
CV_A = 0.5; % coefficient of variation for the cell area
cells_source = 5; % number of cells in the source 
LS = linspace(5,100,20)'; % source lengths 
LP = ncP * diameter; % pattern length

CVfun = @(x) nanstd(x) / nanmean(x);
SEfun = @(x) nanstd(x) / sqrt(sum(~isnan(x)));

fitopt = statset('TolFun', tol, 'TolX', tol);

% k = p, d, D, and all three together
names = {'p', 'd', 'D', 'all'};

dir = 'figS5de';
if not(isfolder(dir))
    mkdir(dir)
end

for k = 1:numel(names)

    filename = [dir '/Mean_ls_change_vs_CV_' names{k} '.csv'];
    if names{k} == 'D'
        filename = [dir '/Mean_ls_change_vs_CV_Diff.csv'];
    end

    % Variation in kinetic parameters lets the mean gradient length drift (Vetter & Iber, 2021).
    % To control the ratio mu_d/mu_lambda, mu_lambda needs to be adjusted, such that
    % the effective fitted mu_lambda is constant.
    if names{k} == 'p'
        mu_lambda_adjusted = mu_lambda;
    elseif names{k} == 'd'
        mu_lambda_adjusted = mu_lambda / (1 + 0.435 * CV^2)^0.080;
    elseif names{k} == 'D'
        mu_lambda_adjusted = mu_lambda / (1 - 0.003 * CV + 1.045 * CV^2 - 0.113 * CV^3 + 0.0043 * CV^4)^-0.471;
    elseif strcmp(names{k}, 'all')
        mu_lambda_adjusted = mu_lambda / (1 - 0.011 * CV + 1.355 * CV^2 - 0.179 * CV^3 + 0.0077 * CV^4)^-0.357;
    end

    % define degradation and production rate
    mu_d = mu_D/mu_lambda_adjusted^2; % mean morphogen degradation rate [1/s]
    mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]

    lambda = NaN(length(LS), 1);
    lambda_SE = NaN(length(LS), 1);
    C0 = NaN(length(LS), 1);
    C0_SE = NaN(length(LS), 1);
    CV_lambda = NaN(length(LS), 1);
    CV_lambda_SE = NaN(length(LS), 1);
    CV_0 = NaN(length(LS), 1);
    CV_0_SE = NaN(length(LS), 1);

    % loop over cell diameters
    for i = 1:length(LS)

        fitted_lambda = NaN(nruns, 1);
        fitted_C0 = NaN(nruns, 1);

        diameter_ls = LS(i)/cells_source; % average diameter of cell in ls 

        % loop over several independent runs
        for j = 1:nruns

            % build the domain with variable cell sizes
            [l_s, l_p] = build_domain(LS(i), LP, diameter_ls, diameter, CV_A, cells_source);

            % create grid for the solver
            x0 = [-l_s, 0, l_p];
            x0 = sort([x0 x0(2:end-1)]); % duplicate cell boundary nodes

            ncS = length(l_s);
            ncP = length(l_p);
            nc = ncS + ncP;

            options = bvpset('Vectorized', 'on', 'NMax', 100*nc, 'RelTol', tol, 'AbsTol', tol);

            % default kinetic parameters
            p = mu_p * ones(nc, 1);
            d = mu_d * ones(nc, 1);
            D = mu_D * ones(nc, 1);

            % draw random kinetic parameters for each cell
            if k == 1 || k == 4
                p = random(helper_functions.logndist(mu_p, CV), nc, 1);
            end
            if k == 2 || k == 4
                d = random(helper_functions.logndist(mu_d, CV), nc, 1);
            end
            if k == 3 || k == 4
                D = random(helper_functions.logndist(mu_D, CV), nc, 1);
            end

            % get initial solution
            sol0 = bvpinit(x0, @helper_functions.y0);

            % define new function handles for boundary condition
            % function and initial condition function
            bcfun_init = @(ya, yb) helper_functions.bcfun(ya, yb, nc);

            % define function handle for the diffusion equation
            odefun_init = @(x,y,c) helper_functions.odefun(x, y, c, D, p, d, ncS);

            % solve the equation
            sol = bvp4c(odefun_init, bcfun_init, sol0, options);

            % fit an exponential in log space in the patterning domain
            idx = find(sol.x >= 0);

            param = polyfit(sol.x(idx), log(sol.y(1,idx)), 1);
            fitted_lambda(j) = -1/param(1);
            fitted_C0(j) = exp(param(2));

            % fit a hyperbolic cosine in log space in the patterning domain
            logcosh = @(p,x) p(2) + log(cosh((LP-x)/p(1)));
            mdl = fitnlm(sol.x(idx), log(sol.y(1,idx)), logcosh, [fitted_lambda(j) log(fitted_C0(j)) - log(cosh(LP/fitted_lambda(j)))], 'Options', fitopt);
            fitted_lambda(j) = mdl.Coefficients.Estimate(1);
            fitted_C0(j) = exp(mdl.Coefficients.Estimate(2)) * cosh(LP/fitted_lambda(j));

        end

        % determine the CV of the decay length and the amplitude over the independent runs + SE
        lambda(i) = mean(fitted_lambda);
        lambda_SE(i) = SEfun(fitted_lambda);
        C0(i) = mean(fitted_C0);
        C0_SE(i) = SEfun(fitted_C0);
        CV_lambda(i) = CVfun(fitted_lambda);
        CV_lambda_SE(i) = std(bootstrp(nboot, CVfun, fitted_lambda));
        CV_0(i) = CVfun(fitted_C0);
        CV_0_SE(i) = std(bootstrp(nboot, CVfun, fitted_C0));
    end

    writetable(table(LS, lambda, lambda_SE, C0, C0_SE, CV_lambda, CV_lambda_SE, CV_0, CV_0_SE), filename)

end

 % builds a one-dimensional domain consisting of variably-sized cells
function [l_s, l_p] = build_domain(LS, LP, mu_d, mu_d_ls,  CV_A, cells_source)

    % Inupt parameters:
    % LS =      length of source domain
    % LP =      length of patterning dmoain
    % mu_d =    mean cell diameter
    % CV_A =    coefficient of variation for cell areas

    % mean cell area of patterning domain 
    mu_A = pi * (mu_d/2)^2 * (CV_A^2+1)^(1/4);
    % mean cell area of soucre domain 
    mu_A_ls=  pi * (mu_d_ls/2)^2 * (CV_A^2+1)^(1/4);
    
    % Build the source domain
    l_s = [];
    
    % each source domain has the same number of cells 
    for cell = 1:cells_source
        A = random(helper_functions.logndist(mu_A_ls, CV_A), 1, 1);
        diam = 2*sqrt(A/pi);
        l_s = [l_s, diam];
    end

    % calculate the normalised diameter for each cell
    l_s = fliplr(cumsum(l_s / sum(l_s) * LS));

    % Build the patterning domain 
    l_p = [];

    while sum(l_p) < LP
        A = random(helper_functions.logndist(mu_A, CV_A), 1, 1);
        diam = 2*sqrt(A/pi);
        l_p = [l_p, diam];
    end

    % remove the last cell if the domain length is closer to the target without it
    if length(l_p) > 1 && abs(sum(l_p) - LP) >= abs(sum(l_p) - l_p(end) - LP)
        l_p = l_p(1:end-1);
    end

    % calculate the normalised diameter for each cell
    l_p = cumsum(l_p / sum(l_p) * LP);

end

end
